# webpages

The repository contains the Hugo markdown and other sources of the website for sudo-iot pages hosted on Codeberg.

All content is licensed under the terms of CC BY-SA v4.0 and source code is licensed with the GPL v3+. Code snippets and examples in the content may be licensed separately. The content is Copyright (C) 2021 Simon Butler. 


