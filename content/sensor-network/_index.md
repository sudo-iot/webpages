---
title: Sensor Network
date: 
draft: false
spdx: CC-BY-SA-4.0
author: Simon Butler
description: A description of a demonstration of an open hardware and open source software internet of things sensor network.
# SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
# SPDX-License-Identifier: CC-BY-SA-4.0
---
## Introduction
This website describes the implementation of a demonstration of an open hardware and open source software internet of things (IoT) sensor network. 

The demonstration is being created as part of the activities of the IoT work package of the SUDO project. The sensor network is intended to demonstrate:

* Implementation of sensor networked systems on a variety of open hardware microprocessor and micorocontroller boards based on ARM, AVR, and RISC-V processors;
* Implementation of an interoperable IoT network for data gathering and processing using open source software operating systems and communications protocols;
* The practicality of using microprocessor based edge systems in sensor networks; and 
* That open source hardware and software can be used in the implementation of industrial sensor networks. 

The demonstration consists of a sensor network. The core of the network will be maintained in the University of Skövde, with additional data gathering devices at locations in Skövde and Töreboda, in Sweden. 

## Information About Each Device and Components 
Details of the devices used in the demonstration, and the hardware and software components used, can be found on the following pages. 

* [Components](/components/)
* [Software](/software/)

