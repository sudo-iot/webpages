---
title: Legal and Licensing Information
date: 
draft: false
spdx: CC-BY-SA-4.0
author: Simon Butler
description: Licensing and legal information for website users. 
# SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
# SPDX-License-Identifier: CC-BY-SA-4.0
---
## Cookies and Tracking
This website is implemented using [Hugo](https://gohugo.io/), an open source software content management system, and some local revisions to the [Smol theme](https://themes.gohugo.io/smol/). Smol is a minimalist theme that does not perform any user tracking, contains no JavaScript, and has no external dependencies. We have not added any user tracking cookies, or client or server side usage tracking.

The website is hosted by [Codeberg](https://codeberg.org/) whose terms and conditions imply that usage of the service is monitored on the server side.

## Software Licences
Hugo and Smol are both licenced under the terms of the [MIT licence](https://opensource.org/licenses/MIT). 

## Copyright
The content of this website was created as part of the [SUDO project](https://www.his.se/en/research/informatics/software-systems-research-group/sudo/). 
Unless indicated otherwise, the content of this website is Copyright &copy; 2021 Simon Butler and is licensed under the terms of the [Creative Commons by Attribution ShareAlike v4.0 International](https://creativecommons.org/licenses/by-sa/4.0/legalcode) (CC-BY-SA). 

The source of the website is in the version control repository at [https://codeberg.org/sudo-iot-webpages](https://codeberg.org/sudo-iot-webpages).
