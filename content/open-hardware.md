---
title: Open Hardware
date: 
draft: false
spdx: CC-BY-SA-4.0
author: Simon Butler
description: A short descripttion of open hardware and a summary of open hardware licensing.
# SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
# SPDX-License-Identifier: CC-BY-SA-4.0
---

## Introduction
The Open Source Hardware Association defines open hardware (OH) as:

{{< block-quote 
quote="Open Source Hardware (OSHW) is a term for tangible artifacts — machines, devices, or other physical things — whose design has been released to the public in such a way that anyone can make, modify, distribute, and use those things." 
source="[Open Source Hardware Association definition](https://www.oshwa.org/definition/)" >}}

The open aspect of OH concerns the *design* of the hardware or physical artefact.
The principles of the OSHWA's basic definition introduce requirements for both the licensing of the design and for the phsical components used. The design must be licensed in a way that is similar to open source software, and the physical components must be available to be used. The availability of physical components for use can be difficult to define, but a working definition might be that there are no restrictions on the use of the component, i.e. that *the component can be acquired and used without restriction*.  It can also be helpful to remember that the concept of open hardware applies to any physical object, and that the narrow focus on electronic devices in this particular context can be unhelpful.

## Licensing
The development of OH has been accompanied by the development of OH licences. Most licences draw on approaches and lessons learnt from the licensing of open source software, and can be divided into two broad types of licence: permissive and reciprocal. For example, the [SolderPad Licence v2.1](http://solderpad.org/licenses/SHL-2.1/) is a permissive licence that applies the principles of the well-known and widely used [Apache v2.0](https://www.apache.org/licenses/LICENSE-2.0) open source software licence. Version 2 of the [CERN Open Hardware Licence](https://cern-ohl.web.cern.ch/home), developed more recently than the SolderPad Licence, provides three options for hardware designers: a permissive version, and weakly and strongly reciprocal versions. 


## Links and References
* [Open Source Hardware Association](https://www.oshwa.org/)
* [CERN Open Hardware Licence](https://cern-ohl.web.cern.ch/home)
* [SolderPad Licence v2.1](http://solderpad.org/licenses/SHL-2.1/)

{{< disclaimer >}}
Please note that this page is intended to be an introductory guide to open hardware that summarises details from other sources. Accordingly it should not be considered to be an authoritative statement.
{{</ disclaimer >}}
