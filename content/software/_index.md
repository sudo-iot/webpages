---
title: "Software Components"
date: 
draft: false
spdx: CC-BY-SA-4.0
author: Simon Butler
description: A list of software components used in projects implemented by the IoT work package of the SUDO project. 
# SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
# SPDX-License-Identifier: CC-BY-SA-4.0
---


The following software components are used in hardware and systems developed as part of the IoT work package of the SUDO project. 
