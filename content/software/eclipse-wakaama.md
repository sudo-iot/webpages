---
title: Eclipse Wakaama
date: 
draft: false
spdx: CC-BY-SA-4.0
author: Simon Butler
description: A summary of the software created by the Eclipse Wakaama open source software project. 
# SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
# SPDX-License-Identifier: CC-BY-SA-4.0
---
The [Eclipse Wakaama](https://eclipse.org/wakaama/) project develops an open source implementation of a client for the Open mobile Alliance's [Lightweight Machine to Machine (LwM2M) protocol](https://omaspecworks.org/what-is-oma-specworks/iot/lightweight-m2m-lwm2m/).

## Licensing
[Eclipse Wakaama](https://eclipse.org/wakaama/) is released under the terms of the [Eclipse Public License v2](https://www.eclipse.org/legal/epl-2.0/) and the [Eclipse Distribution License v1.0](https://www.eclipse.org/org/documents/edl-v10.php).

## Technical Description
(to be added)

## Further Information
* [Eclipse Wakaama project page](https://eclipse.org/wakaama/)
* [GitHub repository](https://github.com/eclipse/wakaama)
* [OMA LwM2M](https://omaspecworks.org/what-is-oma-specworks/iot/lightweight-m2m-lwm2m/)
