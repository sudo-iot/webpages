---
title: Eclipse Leshan
date: 
draft: false
spdx: CC-BY-SA-4.0
author: Simon Butler
description:
# SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
# SPDX-License-Identifier: CC-BY-SA-4.0
---
## Introduction
The [Eclipse Leshan](https://eclipse.org/leshan/) project develops open source implementations of both client and server for the Open mobile Alliance's [Lightweight Machine to Machine (LwM2M) protocol](https://omaspecworks.org/what-is-oma-specworks/iot/lightweight-m2m-lwm2m/).

## Licensing
[Eclipse Leshan](https://eclipse.org//leshan/) is released under the terms of the [Eclipse Public License v2](https://www.eclipse.org/legal/epl-2.0/) and the [Eclipse Distribution License v1.0](https://www.eclipse.org/org/documents/edl-v10.php).

## Technical Description
(to be added)

## Further Information
* [Eclipse Leshan project page](https://eclipse.org/leshan/)
* [GitHub repository](https://github.com/eclipse/leshan)
* [OMA LwM2M](https://omaspecworks.org/what-is-oma-specworks/iot/lightweight-m2m-lwm2m/)

