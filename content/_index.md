---
title: "SUDO-IoT"
date: 
draft: false
spdx: CC-BY-SA-4.0
author: Simon Butler
description: A brief description of the SUDO project and the IoT work package in SUDO.
# SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
# SPDX-License-Identifier: CC-BY-SA-4.0
---
# SUDO IoT

SUDO is a four year collaboration between the Software Systems Research Group at the Universiy of Skövde and six software-intensive businesses in Sweden. SUDO investigates challenges in standardisation and software development to identify practices and strategies that support sustainable digitalisation. 
Partner companies in the SUDO project are Combitech AB, Digitalist Sweden AB, Husqvarna AB, PrimeKey Solutions AB, RedBridge AB, and Scania CV AB.

The IoT work package in the SUDO project focuses on the application of open standards in IoT, and the development and use of [open hardware (OH)](/open-hardware/) and open source software (OSS) solutions in industrial IoT. As part of the research in the SUDO project, members build open hardware demonstrations that are documented in these pages, and in repositories at [https://codeberg.org/sudo-iot/](https://codeberg.org/sudo-iot/)
