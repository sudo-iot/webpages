---
title: Photoresistor Module
date: 
draft: false
spdx: CC-BY-SA-4.0
author: Simon Butler
description: Details of the photoresistor module.
# SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
# SPDX-License-Identifier: CC-BY-SA-4.0
---
## Introduction
The photoresistor module is used in early prototypes of a sensor board to measure light levels. 

## Application: software and function
The component consists of a photoresistor that allows more current to pass related to the level of ambient light. In early prototypes the value reported by the sensor is used without modification. There are plans to investigate the practicality of calibrating the device so that the values returned might be interpreted more meaningfully.

The intention is to replace the photoresistor module with digital light sensor in later iterations of the sensor board. 

## Links
* [Photoresistor module documentation](https://www.electrokit.com/uploads/productfile/41015/41015727_-_Photoresistor_Module.pdf)

