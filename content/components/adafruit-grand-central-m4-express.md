---
title: Adafruit Grand Central M4 Express
date: 
draft: false
spdx: CC-BY-SA-4.0
author: Simon Butler
description: Details and description of the Adafruit Grand Central M4 Express.
# SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
# SPDX-License-Identifier: CC-BY-SA-4.0
---
## Introduction
The [Adafruit Grand Central M4 Express](https://www.adafruit.com/product/4064) is an Arduino mega format MCU board with an [ATSAMD51](https://www.microchip.com/wwwproducts/en/ATSAMD51P20A) CPU, which is a 32-bit ARM Cortex M4 processor. 

## Licensing Information
The Adafruit Grand Central M4 Express is open hardware certified by the OSHWA - certificate reference [US000247](https://certification.oshwa.org/us000247.html)


## Application: software and function
The Grand Central MCU will be deployed in a location away from the main demonstration, and will be used to test the second iteration of the sensor board prototype. Given that there are no network communications as part of the Grand Central, it is expected to be used with an Arduino Ethernet shield.


## Links
* [AdaFruit product page](https://www.adafruit.com/product/4064)
* AdaFruit [overview of Grand Central M4 Express](https://learn.adafruit.com/adafruit-grand-central)
* [Microchip ATSAMD51P20A CPU](https://www.microchip.com/wwwproducts/en/ATSAMD51P20A)

