---
title: AdaFruit Feather M0 Express
date: 
draft: false
spdx: CC-BY-SA-4.0
author: Simon Butler
description: Details of the AdaFruit Feather M0 Express.
# SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
# SPDX-License-Identifier: CC-BY-SA-4.0
---
## Introduction
The AdaFruit Feather M0 Express is a small (feather) format MCU with an [ATSAMD21 Cortex M0](https://www.microchip.com/en-us/product/ATsamd21g18) CPU (ARM Cortex M0). 

## Licensing Information

Open Source Hardware Association certification UID [US000237](https://certification.oshwa.org/us000237.html) 




## Application: software and function
The feather MCU will be deployed in a location away from the main demonstration, and will be used to test the second iteration of the sensor board prototype in conjunction with an [AdaFruit Ethernet FeatherWing](https://www.adafruit.com/product/3201). 

## Links
* [AdaFruit product page](https://www.adafruit.com/product/3403)
