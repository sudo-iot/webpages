---
title: Olimex A64 OLinuXino-1Ge 16GW
date: 
draft: false
spdx: CC-BY-SA-4.0
author: Simon Butler
description: Details of the Olimex A64 OLinuXino-1Ge 16GW.
# SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
# SPDX-License-Identifier: CC-BY-SA-4.0
---
## Introduction 
Olimex A64 OLinuXino-1Ge 16GW is a microprocessor board with an ARM A64 CPU, 1GB of RAM, and WiFi/Bluetooth Low Energy (BLE) capabilities as well as Ethernet. The board will run Linux distrbutions and, similar to the more familiar Raspberry Pi, can be connected to a monitor, keyboard and mouse and used as a desktop computer.

## Licensing information
Open Source Hardware with [Certification UID BG000042](https://certification.oshwa.org/bg000042.html).

## Application: software and function
The A64 currently runs the Ubuntu Linux operating system and is intended be used as an edge device aggregating data from a group of sensors in one location. 

## Links

* [Olimex product page](https://www.olimex.com/Products/OLinuXino/A64/A64-OLinuXino/open-source-hardware)

