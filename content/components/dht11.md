---
title: DHT11 Temperature and Humidity sensor
date: 
draft: false
spdx: CC-BY-SA-4.0
author: Simon Butler
description: Details of the DHT11 temperature and humidity sensor.
# SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
# SPDX-License-Identifier: CC-BY-SA-4.0
---
## Introduction
The DHT11 is a simple integrated temperature and humidity sensor. The DHT11 has limitations, most notably in accuracy as the temperature sensor has an error of +/- 2C and the humidty sensor an error of +/- 5% relative humidity. 

## Application: software and function
The DHT11 is used at an early stage of prototyping, primarily as a humidity sensor, because it is easily available. The DHT11 as a standalone sensor, or mounted on a breakout board. Both are used in the prototype sensor boards and as a result there are two variants of the circuit design for the prototype sensor board.

## Links
* [DHT11 tutorial](https://lastminuteengineers.com/dht11-module-arduino-tutorial/)
* [DHT11 and DHT22 tutorial](https://lastminuteengineers.com/dht11-dht22-arduino-tutorial/)
* [DHT library](https://github.com/RobTillaart/Arduino/tree/master/libraries/DHTlib)
* [DHT New library](https://github.com/RobTillaart/DHTNEW) 
