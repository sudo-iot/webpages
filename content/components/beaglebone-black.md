---
title: BeagleBone Black
date: 
draft: false
spdx: CC-BY-SA-4.0
author: Simon Butler
description: information about the Beaglebone Black microprocessor board and how it is used in the sensor network demonstration. 
# SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
# SPDX-License-Identifier: CC-BY-SA-4.0
---
## Introduction
The [BeagleBone Black](https://beagleboard.org/black) is a microprocessor board with an ARM Cortex A8 processor. 

## Licensing Information
The [BeagleBone Black](https://beagleboard.org/black) is open hardware certified by the OSHWA - certificate reference [US000236](https://certification.oshwa.org/us000236.html)


## Application: software and function
The BeagleBone Black used runs the Debian Linux operating system and provides network services, including dynamic hostname control protocol (DHCP) for some other machines in the demonstration. The machine is also responsible for the aggregation of data and runs an instance of the Eclipse Leshan LwM2M server to collect data from MCUs with sensors. 
It also communicates with a server in a cloud instance to obtain data from sensor machines in remote locations. 

## Further Information
* [Beaglebone Black product page](https://beagleboard.org/black)

