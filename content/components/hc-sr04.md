---
title: HC-SR04 - Ultrasonic sensor
date: 
draft: false
spdx: CC-BY-SA-4.0
author: Simon Butler
description: Details of the HC-SR04 ultrasonic sensor.
# SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
# SPDX-License-Identifier: CC-BY-SA-4.0
---
## Introduction
The HC-SR04 is an ultrasonic sensor consisting of a transmitter and a receiver that can be used to detect objects and measure the distance to them up to 4m from the device. 


## Application: software and function
The HC-SR04 is a candidate component for use in the sensor networks. Further investigation of the licensing of software libraries available to manage the device is required before a final decision can be made whether to adopt it for use.

## Links
* [HC-SR04 data sheet](https://cdn.sparkfun.com/datasheets/Sensors/Proximity/HCSR04.pdf)
* [Tutorial at Last Minute Engineers](https://lastminuteengineers.com/arduino-sr04-ultrasonic-sensor-tutorial/)
* [Tutorial at Random Nerd](https://randomnerdtutorials.com/complete-guide-for-ultrasonic-sensor-hc-sr04/)

