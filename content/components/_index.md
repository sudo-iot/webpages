---
title: "Components"
date: 
draft: false
spdx: CC-BY-SA-4.0
author: Simon Butler
description: A list of links to pages about hardware and software components used in projects implemented as part of the IoT work package of the SUDO project. 
# SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
# SPDX-License-Identifier: CC-BY-SA-4.0
---

The following components are used in the development of systems by the IoT work package in the SUDO project. 
