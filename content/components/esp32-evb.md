---
title: Olimex ESP32 Evaluation Board
date: 
draft: false
spdx: CC-BY-SA-4.0
author: Simon Butler
description: Details of the Olimex ESP32 evaluation board.
# SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
# SPDX-License-Identifier: CC-BY-SA-4.0
---
## Introduction
The ESP32 is a chip manufactured by [Espressif Systems Co., Ltd](https://www.espressif.com/) that includes Bluetooth and WiFi capabilities and used in mobile devices. In the demonstration, the ESP32 is used in the form of [an evaluation board manufactured by Olimex](https://www.olimex.com/Products/IoT/ESP32/ESP32-EVB/open-source-hardware). 
The board deployed in the demonstration is [Revision I](https://github.com/OLIMEX/ESP32-EVB/tree/master/HARDWARE/REV-I) of the product.

## Licensing Information
The Olimex ESP32-EVB is certified by the 
Open Source Hardware Association (Certification UID [BG000011](https://certification.oshwa.org/bg000011.html)).

## Application: software and function
The ESP32-EVB is used to demonstrate a sensor device and the use of the OMA Lightweight Machine to Machine (LwM2M) protocol. The sensor device is implemented on a 'breadboard' so that the components of the device can be seen clearly. A functionally similar circuit will be implemented on boards for the feather and conventional Arduino form factors, the schema and bill of materials for the circuit and boards can be found in the repository at [https://codeberg.org/sudo-iot/sensor-board-prototype](https://codeberg.org/sudo-iot/sensor-board-prototype). 

The software reads data from the sensors in the circuit board and uses [Eclipse Wakaama](/software/eclipse-wakaama/) to transmit data over a wired Ethernet connection to an [Eclipse Leshan server](/software/eclipse-leshan/) that runs on the [Beaglebone Black](/components/beaglebone-black/) board in the demonstration. 

## Links

* [Olimex product page](https://www.olimex.com/Products/IoT/ESP32/ESP32-EVB/open-source-hardware)
* [Open Source Hardware Association certification ](https://certification.oshwa.org/bg000011.html)
* Olimex [GitHub repository for board](https://github.com/OLIMEX/ESP32-EVB)
* Olimex [GitHub pages for revision I](https://github.com/OLIMEX/ESP32-EVB/tree/master/HARDWARE/REV-I)
* [Schematic (PDF)](https://github.com/OLIMEX/ESP32-EVB/blob/master/HARDWARE/REV-I/ESP32-EVB_Rev_I.pdf)
