---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
spdx: CC-BY-SA-4.0
# SPDX-FileCopyrightText: 2021 Simon Butler <simon.butler@his.se>
# SPDX-License-Identifier: GPL-3.0-or-later
---

